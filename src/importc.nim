import macros,tables,strutils,os,hashes,importc/utils

const ENV_Nim_C_Include_Path:string = getEnv("NIM_C_INCLUDE_PATH","")
const Append_Nim_C_Include_Path {.strdefine.}:string = ""
const Nim_C_Include_Path* {.strdefine.}:string = getEnv("USERPROFILE") & "/.choosenim/toolchains/mingw64/x86_64-w64-mingw32/include" & ";" & Append_Nim_C_Include_Path & ENV_Nim_C_Include_Path 
var Current_Module_Path {.compileTime.}:string = ""

type
    IncludeInfo = object
        accessor:string #may need to be a hash
        headerName:string
        location:seq[string]
        manglingProc:NimNode
    IncludeTable = TableRef[string,IncludeInfo]
    ProcReferenceUses = enum
        pruNameMangling,
        pruIncluded
    ProcReferenceList = TableRef[tuple[kind:ProcReferenceUses,hash:Hash],string]
    Module = object
        sourcePath:string #this is the filepath for the current `Nim` module that this `Module` is located in
        includes:IncludeTable
    Header = object
        name:string
        manglingProc:proc(strYouWant:string):string



proc newCIncludeTable(initialSize:int = defaultInitialSize):IncludeTable =
    result = newTable[string,IncludeInfo](initialSize)



proc newModule(path:string):Module =
    result.sourcePath = path
    #result.procReferences = newTable[tuple[kind:ProcReferenceUses,hash:string],pointer]()
    result.includes = newCIncludeTable()


proc newProcReferenceList(initialSize:int = defaultInitialSize):ProcReferenceList= #the return value is the signature hash
    result = newTable[tuple[kind:ProcReferenceUses,hash:Hash],string](initialSize)


var Global_Module_Info {.compileTime.}:TableRef[string,Module] = newTable[string,Module]()
template CurrentIncludes:IncludeTable = Global_Module_Info[Current_Module_Path].includes

proc addModule(modFileName:string):Module =
    var mi:Module = newModule(modFileName)
    Global_Module_Info.add(modFileName,mi)
    return Global_Module_Info[modFileName]

proc stripHeaderStr(headerStr:string):string =
    result = headerStr.strip()
    #detect carrots
    if result[0] == '<' and result[result.len-1] == '>': #the carrots mean something in C, ill have to look that up
        delete result,result.len-1,result.len-1
        delete result,0,0

proc headerExistsWhere*(headerStr:string,firstOccuranceOnly:bool = true):tuple[headerExists:bool,headerLocation:seq[string]] =
    let searchPaths = Nim_C_Include_Path.split(';')
    result.headerExists = false
    #eventually cache search paths per compiled file in nimcache
    for path in searchPaths:
        if path == "":
            continue
        if fileExists((path / headerStr).unixToNativePath()):
            if firstOccuranceOnly:
                return (true,@[(path / headerStr).unixToNativePath])
            else:
                result.headerExists = true
                result.headerLocation.add((path / headerStr).unixToNativePath)

proc headerExists*(headerStr:string):bool =
    return headerExistsWhere(headerStr).headerExists

proc addHeaderReference(headerExpr:NimNode):IncludeInfo =
    #if the header expression is not an infix `as` operation
    #then we will assume they want the header name to be the same as the file
    #eg: "<windows.h>" removes the carrots and the extension
    let exprKind = headerExpr.kind
    var strippedStr = ""
    var accessor:string = ""
    #echo exprKind
    #this handles the processesing of different AST structures to give me the data i want
    case exprKind:
    of nnkInfix:
        ##echo headerExpr.astGenRepr
        expectKind(headerExpr[0],nnkIdent)
        assert(headerExpr[0].repr == "as")
        expectKind(headerExpr[1],nnkStrLit)
        expectKind(headerExpr[2],nnkIdent)
        strippedStr = stripHeaderStr(headerExpr[1].strVal)
        accessor = headerExpr[2].repr
    of nnkStrLit:
        #strip whitespace and carrots from header
        strippedStr = stripHeaderStr(headerExpr.strVal)
        accessor = substr(strippedStr,0,find(strippedStr,'.')-1)
        #see if the file actually exists
    else:
        assert(false,"THIS IS NOT SUPPORTED")
    
    if accessor.contains('/'):
        let possible_accessor = extractFilename(accessor)
        if not CurrentIncludes.hasKey(possible_accessor):
            accessor = possible_accessor
        else:
            accessor = accessor.replace('/','.')

    if not CurrentIncludes.hasKey(accessor):
        let searchResult = headerExistsWhere(strippedStr)
        assert(searchResult.headerExists,"C header: " & strippedStr & " does not exist!")
        let nIncludeInfo:IncludeInfo = IncludeInfo(accessor: accessor,headerName:strippedStr,location:searchResult.headerLocation)
        CurrentIncludes.add(nIncludeInfo.accessor,nIncludeInfo)
        return nIncludeInfo
    else:
        warning("C header, included twice: " & strippedStr)
        return CurrentIncludes[accessor]


macro importC*(headerExprs:varargs[untyped]):untyped =
    Current_Module_Path = headerExprs.lineInfoObj.filename
    var curmod:Module
    var accessors:seq[string] = newSeq[string]()
    if not Global_Module_Info.hasKey(Current_Module_Path):
        curmod = addModule(Current_Module_Path)
    else:
        curmod = Global_Module_Info[Current_Module_Path]
    #what do i want this macro to do?
    #[
        it already adds the string to a reference list, and searches for and returns the path of the header
        I want to expand this to keep track of each header used in each module
        ** I will need a cache list so you are not looking for the same libraries more than once
        i might have it return a "Import object" that will allow for accessors to functions and such
        but for now i think ill keep it simple
    ]#
    for expr in headerExprs:
        accessors.add addHeaderReference(expr).accessor
    
    result = newStmtList()
    for acc in accessors:
        #var osym = newTree(nnkVarSection,newTree(nnkIdentDefs,newTree(nnkSym,newStrLit(acc),)))
        let ident = newIdentNode(acc)
        result.add quote do:
            var `ident`:Header
            `ident`.name = `acc`
            #`ident`.procs = newProcReferenceList()


macro doMangle(h:Header,mproc:untyped):untyped =
    CurrentIncludes[h.repr].manglingProc = mproc


template mangle*(h:Header,mproc:proc(strYouWant:string):string):untyped =
    doMangle(h,mproc)



macro using_header*(header:Header,body:untyped):untyped {.nimcall.}=
    #here we are going to assume you have used the importC statement, just to keep things clean
    let accessor = header.repr
    let includeInfo = CurrentIncludes[accessor]

    for child in body.children:
        case child.kind:
        of nnkProcDef:
           #this is when name mangling should be executed
           if child.body.kind == nnkEmpty:
               # we are assuming you are importing the proc if the body of it is empty
               #otherwise we are skipping over it **for now** I may add in procedures for exporting to C and other languages
               # but not right now
               echo includeInfo
               let mproc = includeInfo.manglingProc
               let name = child.name.repr
               let call  = quote do:
                   `mproc`(`name`)
               child.addPragma(newColonExpr(newIdentNode("importc"),call))
               child.addPragma(newColonExpr(newIdentNode("header"),newStrLitNode(includeInfo.location[0])))
               #let childHash = child.repr.hash
               #let childSym = child.name.repr
               echo body.repr
               #body.add quote do:
                #   `header`.procs.add((pruIncluded,`childHash`),`childSym`)

        else:
            continue
    return body


template implement*(h:var Header,body:untyped):untyped =
    using_header(h,body)

{.push experimental: "dotOperators".}
ignore:
    macro `.`(h:Header,op:proc,args:varargs[untyped]):untyped =
        var opCall:NimNode
        let hname = h.repr
        let opName = op.repr
        if args.len == 0:
            opCall = newCall(op)
        else:
            opCall = newCall(op,args)
        if CurrentIncludes.contains(h.repr):
            result = `opCall`
        else:
            result = quote do:
                assert(false,`hname` & " does not implememnt proc: " & `opName`)
{.pop.}
when isMainModule and false:
    importC "windows.h"
    var doit = 0
    mangle(windows,upperCaseFirstLetter)
    windows.implement:
        proc getCurrentProcessId():culong
    
    echo getCurrentProcessId()
    #windows.includeProc:
    #    proc GetCurrentProcessId():culong
    #windows.addMangling:
    #    mangleString
    #echo windows
    #echo GetCurrentProcessId()
