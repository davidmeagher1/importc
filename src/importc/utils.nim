const disableIgnore {.booldefine.}:bool = false

macro ignore*(b:untyped):untyped =
    if disableIgnore:
        return b
    else:
        discard