# Package

version       = "0.1.0"
author        = "David Meagher"
description   = "A library that allows for easier importing of C headers **update**"
license       = "MIT"
srcDir        = "src"



# Dependencies

requires "nim >= 1.2.6"
